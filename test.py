
import sys
import unittest
from tests.commandeer_common import *
import commandeer


if sys.version_info[0] == 3:
    from tests.commandeer3_tests import *


unittest.main()