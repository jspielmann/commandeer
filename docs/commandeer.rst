commandeer Package
==================

:mod:`commandeer` Package
-------------------------

.. automodule:: commandeer
    :members:
    :undoc-members:
    :show-inheritance:

