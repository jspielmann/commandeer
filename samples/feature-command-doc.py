import commandeer

def test_command():
	"""This is the shortdoc for 'test'
	
	More documentation can be put here. It will only show up when you call the help for this command, i.e. ``feature-command-doc.py help test``.
	It can be as long as you want and should describe what this command does.
	"""
	pass
	
if __name__ == '__main__':
	commandeer.cli()