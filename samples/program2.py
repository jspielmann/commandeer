"""Show the different uses of Commandeer"""

import commandeer

def info_command():
	"""Print some program information
	
	At the moment, this is pretty empty, but we will expand upon that in the future!
	"""
	print("this is my function")

if __name__ == '__main__':
	commandeer.cli()
