This project is deprecated and will not be maintained. Please refer to the excellent [click](https://click.palletsprojects.com/en/8.1.x/) for a similar tool.

Commandeer is a command-line generator library that you can use to add a nice command line interface to your python programs.

It will take care of parsing all command line options as well as calling the right command functions in your script. And it will generate a nice-looking help for the program and for each of the commands.

We've written Commandeer in such a way that it should work just like you expect it to. There are only two things you need to do:
 * name the functions you want to expose on the command line to end in ``_command``.
 * Add the following code snippet to the end of your command line module::

       import commandeer
       if __name__ == '__main__':
           commandeer.cli()

You should try it out!

For more information, consult the documentation on
  https://gitlab.com/jspielmann/commandeer/
  
  
License information
-------------------

Copyright (C) 2012, 2021 Johannes Spielmann

Commandeer is licensed under the terms of the GNU General Public Licenses as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

For a full description of the license, please see the file

    LICENSE
