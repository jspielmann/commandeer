# Copyright (C) 2012 Johannes Spielmann
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Commandeer main functions"""

import sys
import os
import subprocess
import unittest
import commandeer
from tests.commandeer_common import *     # noqa


def example_command(some_param, new_param, other_param=None, *args, **kwargs):
    """A command with a long line at the start of the description, which should actually break when doing documentation.
    
    dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. 
    dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. 
    dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. 
    dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. 
    
    :param some_param: dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. 
    :param other_param: dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. dolorem ipsum sit amet consectetur adiscipit. 
    """
example_command.aliases = 'a long list of aliases that should spill over onto a new line it really should and not stop never'.split()

def test_command(*args):
    """Run tests on commandeer library.

    Simply runs the test cases from the test/ directory. This is really important because then you can use your
    imagination to write more tests and you can be sure that these tests are run!
    """

    argv = args or sys.argv[:1]

    unittest.main(argv=argv)


def doc_command(dryrun=False):
    """Create the documentation for Commandeer.

    Runs the sphinx-apidoc against the commandeer source, builds the html and opens it in a browser. Try it out in a dryrun
    if you want to see what happens.
    Requires that you have Sphinx installed an on your path.
    :param dryrun: If set to True, the program will only output the command lines that would be executed in the shell. If set to False, will run the actual commands.
    """
    testdir = os.path.join(os.getcwd(), 'tests')
    docsdir = os.path.join(os.getcwd(), 'docs')
    if dryrun:
        print("make html", "cwd=" + docsdir)
        print("open docs/_build/html/index.html")
    else:
        subprocess.call("make html", cwd=docsdir, shell=True)
        subprocess.call("open docs/_build/html/index.html", shell=True)


if __name__ == '__main__':
    commandeer.cli()
